/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package aplikasigui;


import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

public class MainFrame extends JFrame {
    private JTable table;
    private DefaultTableModel tableModel;
    private JTextField nameField;
    private JTextField ageField;

    public MainFrame() {
        // mengatur judul 
        setTitle("Database Anggota komunitas");
        setSize(600, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        // Setup table model
        tableModel = new DefaultTableModel();
        tableModel.addColumn("ID");
        tableModel.addColumn("Name");
        tableModel.addColumn("Age");

        // Setup table
        table = new JTable(tableModel);

        
        loadTableData();

        // Setup input panel
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2, 2));

        panel.add(new JLabel("Name:"));
        nameField = new JTextField();
        panel.add(nameField);

        panel.add(new JLabel("Age:"));
        ageField = new JTextField();
        panel.add(ageField);

        // Setup add button
        JButton addButton = new JButton("Add");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addData();
            }
        });

        add(new JScrollPane(table), BorderLayout.CENTER);
        add(panel, BorderLayout.NORTH);
        add(addButton, BorderLayout.SOUTH);
    }
        //memuat data dari database
    private void loadTableData() {
        try (Connection conn = DatabaseConnector.getConnection();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM Anggota")) {

            while (rs.next()) {
                tableModel.addRow(new Object[]{
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getInt("age")
                });
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
     //memambahkan data dari inputan
    private void addData() {
        String name = nameField.getText();
        int age = Integer.parseInt(ageField.getText());

        try (Connection conn = DatabaseConnector.getConnection();
             PreparedStatement stmt = conn.prepareStatement("INSERT INTO Anggota (name, age) VALUES (?, ?)")) {

            stmt.setString(1, name);
            stmt.setInt(2, age);
            stmt.executeUpdate();
            tableModel.addRow(new Object[]{
                    getLastInsertedId(conn), name, age
            });
            nameField.setText("");
            ageField.setText("");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private int getLastInsertedId(Connection conn) throws SQLException {
        int id = -1;
        try (Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT LAST_INSERT_ID()")) {
            if (rs.next()) {
                id = rs.getInt(1);
            }
        }
        return id;
    }
}

